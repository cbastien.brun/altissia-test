# **Altissia project**

Build project :

`mvn clean install`

Build docker image :

`docker-compose build`

Run project :

`docker-compose up`

Endpoint :

http://localhost:8080/fizzbuzz?entry=3,5,7

