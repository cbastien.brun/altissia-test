package org.altissia.services;

import org.altissia.utils.MultipleUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Service
public class FizzBuzzService {


    /**
     * Compute Fizz Buzz Bazz result depending of the entry.
     *
     * @param number : number to compute
     * @return Fizz if multiple of 3 , Buzz if multiple of 5 and Bazz if multiple of 7.
     * If {number} is multiple of 3 and 5 and 7 result will be FizzBuzzBazz
     * If there is no multiple , result will be the number given in entry.
     */
    public String computeFizzBuzzBazz(final Integer number) {
        if(Objects.isNull(number)) {
            // Return Default result ?
            //For null value in the list we return empty string
            return Strings.EMPTY;
        }

        final StringBuilder response = new StringBuilder();
        if (MultipleUtils.isMultipleOf(3, number)) {
            response.append("Fizz");
        }
        if (MultipleUtils.isMultipleOf(5, number)) {
            response.append("Buzz");
        }
        if (MultipleUtils.isMultipleOf(7, number)) {
            response.append("Bazz");
        }
        if (response.isEmpty()) {
            response.append(number);
        }
        return response.toString();
    }

    /**
     * Default Answer when entry is empty or null
     * */
    public List<String> defaultAnswer() {
        return IntStream.range(1,100)
                .mapToObj(number -> this.computeFizzBuzzBazz(number))
                .collect(Collectors.toList());
    }



}
