package org.altissia.utils;

import java.util.Objects;

public class MultipleUtils {


    private MultipleUtils() {
    }


    /**
     * Method to check if {number} is multiple of {multiple}
     *
     * @param multiple : multiple
     * @param number   : number to check
     * @return true if is multiple otherwise return false.
     */
    public static boolean isMultipleOf(final Integer multiple, final Integer number) {
        if (Objects.nonNull(multiple) && Objects.nonNull(number)) {
            return (number % multiple) == 0;
        }
        return false;
    }
}
