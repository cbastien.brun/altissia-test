package org.altissia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationAltissia {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationAltissia.class, args);
    }

}
