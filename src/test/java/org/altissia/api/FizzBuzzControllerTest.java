package org.altissia.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.containsString;

/**
 * Integration TEST
 * */
@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class FizzBuzzControllerTest {


    @Autowired
    private MockMvc mockMvc;



    @Test
    public void shouldReturnDefaultAnswer_1() throws Exception {
        this.mockMvc.perform(get("/fizzbuzz")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\",\"Fizz\",\"Bazz\",\"8\",\"Fizz\",\"Buzz\",\"11\",\"Fizz\",\"13\",\"Bazz\",\"FizzBuzz\",\"16\",\"17\",\"Fizz\",\"19\",\"Buzz\",\"FizzBazz\",\"22\",\"23\",\"Fizz\",\"Buzz\",\"26\",\"Fizz\",\"Bazz\",\"29\",\"FizzBuzz\",\"31\",\"32\",\"Fizz\",\"34\",\"BuzzBazz\",\"Fizz\",\"37\",\"38\",\"Fizz\",\"Buzz\",\"41\",\"FizzBazz\",\"43\",\"44\",\"FizzBuzz\",\"46\",\"47\",\"Fizz\",\"Bazz\",\"Buzz\",\"Fizz\",\"52\",\"53\",\"Fizz\",\"Buzz\",\"Bazz\",\"Fizz\",\"58\",\"59\",\"FizzBuzz\",\"61\",\"62\",\"FizzBazz\",\"64\",\"Buzz\",\"Fizz\",\"67\",\"68\",\"Fizz\",\"BuzzBazz\",\"71\",\"Fizz\",\"73\",\"74\",\"FizzBuzz\",\"76\",\"Bazz\",\"Fizz\",\"79\",\"Buzz\",\"Fizz\",\"82\",\"83\",\"FizzBazz\",\"Buzz\",\"86\",\"Fizz\",\"88\",\"89\",\"FizzBuzz\",\"Bazz\",\"92\",\"Fizz\",\"94\",\"Buzz\",\"Fizz\",\"97\",\"Bazz\",\"Fizz\"]")));
    }

    @Test
    public void shouldReturnDefaultAnswer_2() throws Exception {
        this.mockMvc.perform(get("/fizzbuzz?entry=")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\",\"Fizz\",\"Bazz\",\"8\",\"Fizz\",\"Buzz\",\"11\",\"Fizz\",\"13\",\"Bazz\",\"FizzBuzz\",\"16\",\"17\",\"Fizz\",\"19\",\"Buzz\",\"FizzBazz\",\"22\",\"23\",\"Fizz\",\"Buzz\",\"26\",\"Fizz\",\"Bazz\",\"29\",\"FizzBuzz\",\"31\",\"32\",\"Fizz\",\"34\",\"BuzzBazz\",\"Fizz\",\"37\",\"38\",\"Fizz\",\"Buzz\",\"41\",\"FizzBazz\",\"43\",\"44\",\"FizzBuzz\",\"46\",\"47\",\"Fizz\",\"Bazz\",\"Buzz\",\"Fizz\",\"52\",\"53\",\"Fizz\",\"Buzz\",\"Bazz\",\"Fizz\",\"58\",\"59\",\"FizzBuzz\",\"61\",\"62\",\"FizzBazz\",\"64\",\"Buzz\",\"Fizz\",\"67\",\"68\",\"Fizz\",\"BuzzBazz\",\"71\",\"Fizz\",\"73\",\"74\",\"FizzBuzz\",\"76\",\"Bazz\",\"Fizz\",\"79\",\"Buzz\",\"Fizz\",\"82\",\"83\",\"FizzBazz\",\"Buzz\",\"86\",\"Fizz\",\"88\",\"89\",\"FizzBuzz\",\"Bazz\",\"92\",\"Fizz\",\"94\",\"Buzz\",\"Fizz\",\"97\",\"Bazz\",\"Fizz\"]")));
    }

    @Test
    public void isValidEntryAndMultipleOf3() throws Exception {
        this.mockMvc.perform(get("/fizzbuzz?entry=3")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("[\"Fizz\"]")));
    }

    @Test
    public void isValidEntryAndArray() throws Exception {
        this.mockMvc.perform(get("/fizzbuzz?entry=3,5")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("[\"Fizz\",\"Buzz\"]")));
    }

    @Test
    public void isValidEntryAndArrayWithNull() throws Exception {
        this.mockMvc.perform(get("/fizzbuzz?entry=3,5,,7")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(equalTo("[\"Fizz\",\"Buzz\",\"\",\"Bazz\"]")));
    }

    @Test
    public void errorCase() throws Exception {
        this.mockMvc.perform(get("/fizzbuzz?entry=test")).andDo(print()).andExpect(status().is4xxClientError());
    }

}
