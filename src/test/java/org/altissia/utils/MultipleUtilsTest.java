package org.altissia.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MultipleUtilsTest {


    @Test
    public void isMultipleof_nullValue() {
        Assert.assertFalse(MultipleUtils.isMultipleOf(null, null));
    }

    @Test
    public void isMultipleof_nullMultiple() {
        Assert.assertFalse(MultipleUtils.isMultipleOf(null, 15));
    }

    @Test
    public void isMultipleof_nullNumber() {
        Assert.assertFalse(MultipleUtils.isMultipleOf(5, null));
    }

    @Test
    public void isMultipleof_15() {
        Assert.assertTrue(MultipleUtils.isMultipleOf(15, 30));
    }

}
