package org.altissia.service;


import org.altissia.services.FizzBuzzService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FizzBuzzServiceTest {

    @Autowired
    private FizzBuzzService fizzBuzzService;

    @Test
    public void computeFizzBuzzNumber_NullValue(){
        String result = fizzBuzzService.computeFizzBuzzBazz(null);
        Assert.assertEquals("", result);
    }

    @Test
    public void computeFizzBuzzNumber_isMultipleOf3(){
        String result = fizzBuzzService.computeFizzBuzzBazz(3);
        Assert.assertEquals("Fizz", result);
    }

    @Test
    public void computeFizzBuzzNumber_isMultipleOf5(){
        String result = fizzBuzzService.computeFizzBuzzBazz(5);
        Assert.assertEquals("Buzz", result);
    }

    @Test
    public void computeFizzBuzzNumber_isMultipleOf7(){
        String result = fizzBuzzService.computeFizzBuzzBazz(7);
        Assert.assertEquals("Bazz", result);
    }


    @Test
    public void computeFizzBuzzNumber_isMultipleOf3and5(){
        String result = fizzBuzzService.computeFizzBuzzBazz(15);
        Assert.assertEquals("FizzBuzz", result);
    }

    @Test
    public void computeFizzBuzzNumber_isMultipleOf3and7(){
        String result = fizzBuzzService.computeFizzBuzzBazz(15);
        Assert.assertEquals("FizzBuzz", result);
    }

    @Test
    public void computeFizzBuzzNumber_isMultipleOf5and7(){
        String result = fizzBuzzService.computeFizzBuzzBazz(35);
        Assert.assertEquals("BuzzBazz", result);
    }

    @Test
    public void computeFizzBuzzNumber_isMultipleOf3and5and7(){
        String result = fizzBuzzService.computeFizzBuzzBazz(105);
        Assert.assertEquals("FizzBuzzBazz", result);
    }


}
